<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.11.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>alienscene.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">alienscene/alien/alien1-1.png</key>
            <key type="filename">alienscene/alien/alien1-2.png</key>
            <key type="filename">alienscene/alien/alien1-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,37,96,74</rect>
                <key>scale9Paddings</key>
                <rect>48,37,96,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/alien/alien2-1.png</key>
            <key type="filename">alienscene/alien/alien2-2.png</key>
            <key type="filename">alienscene/alien/alien2-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,34,76,68</rect>
                <key>scale9Paddings</key>
                <rect>38,34,76,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/alien1-head.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,20,36,40</rect>
                <key>scale9Paddings</key>
                <rect>18,20,36,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/alien2-head.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,11,36,21</rect>
                <key>scale9Paddings</key>
                <rect>18,11,36,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/animals/dog/dog-01.png</key>
            <key type="filename">alienscene/animals/dog/dog-03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,24,95,48</rect>
                <key>scale9Paddings</key>
                <rect>48,24,95,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/animals/dog/dog-02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,26,95,51</rect>
                <key>scale9Paddings</key>
                <rect>48,26,95,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>256,160,512,320</rect>
                <key>scale9Paddings</key>
                <rect>256,160,512,320</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/button-start/start-1.png</key>
            <key type="filename">alienscene/button-start/start-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,28,112,56</rect>
                <key>scale9Paddings</key>
                <rect>56,28,112,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/explosion-fail/explosion-fail-1.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-2.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-3.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-4.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-5.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-6.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-7.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail-8.png</key>
            <key type="filename">alienscene/explosion-fail/explosion-fail.psd</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,36,72,72</rect>
                <key>scale9Paddings</key>
                <rect>36,36,72,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/explosion/explosion0.png</key>
            <key type="filename">alienscene/explosion/explosion1.png</key>
            <key type="filename">alienscene/explosion/explosion2.png</key>
            <key type="filename">alienscene/explosion/explosion3.png</key>
            <key type="filename">alienscene/explosion/explosion4.png</key>
            <key type="filename">alienscene/explosion/explosion5.png</key>
            <key type="filename">alienscene/explosion/explosion6.png</key>
            <key type="filename">alienscene/explosion/explosion7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,48,105,96</rect>
                <key>scale9Paddings</key>
                <rect>52,48,105,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/font/A.png</key>
            <key type="filename">alienscene/font/B.png</key>
            <key type="filename">alienscene/font/C.png</key>
            <key type="filename">alienscene/font/D.png</key>
            <key type="filename">alienscene/font/E.png</key>
            <key type="filename">alienscene/font/F.png</key>
            <key type="filename">alienscene/font/G.png</key>
            <key type="filename">alienscene/font/H.png</key>
            <key type="filename">alienscene/font/I.png</key>
            <key type="filename">alienscene/font/J.png</key>
            <key type="filename">alienscene/font/K.png</key>
            <key type="filename">alienscene/font/M.png</key>
            <key type="filename">alienscene/font/N.png</key>
            <key type="filename">alienscene/font/O.png</key>
            <key type="filename">alienscene/font/P.png</key>
            <key type="filename">alienscene/font/Q.png</key>
            <key type="filename">alienscene/font/R.png</key>
            <key type="filename">alienscene/font/S.png</key>
            <key type="filename">alienscene/font/T.png</key>
            <key type="filename">alienscene/font/U.png</key>
            <key type="filename">alienscene/font/V.png</key>
            <key type="filename">alienscene/font/W.png</key>
            <key type="filename">alienscene/font/X.png</key>
            <key type="filename">alienscene/font/Y.png</key>
            <key type="filename">alienscene/font/Z.png</key>
            <key type="filename">alienscene/number/0.png</key>
            <key type="filename">alienscene/number/1.png</key>
            <key type="filename">alienscene/number/2.png</key>
            <key type="filename">alienscene/number/3.png</key>
            <key type="filename">alienscene/number/4.png</key>
            <key type="filename">alienscene/number/6.png</key>
            <key type="filename">alienscene/number/7.png</key>
            <key type="filename">alienscene/number/8.png</key>
            <key type="filename">alienscene/number/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,12,20,24</rect>
                <key>scale9Paddings</key>
                <rect>10,12,20,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/font/L.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,20,23</rect>
                <key>scale9Paddings</key>
                <rect>10,11,20,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/light-1.png</key>
            <key type="filename">alienscene/light-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,7,36,14</rect>
                <key>scale9Paddings</key>
                <rect>18,7,36,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/logo.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>132,35,264,70</rect>
                <key>scale9Paddings</key>
                <rect>132,35,264,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/number/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,13,20,25</rect>
                <key>scale9Paddings</key>
                <rect>10,13,20,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/replay/button-replay-1.png</key>
            <key type="filename">alienscene/replay/button-replay-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,20,114,40</rect>
                <key>scale9Paddings</key>
                <rect>57,20,114,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/star/star1.png</key>
            <key type="filename">alienscene/star/star10.png</key>
            <key type="filename">alienscene/star/star11.png</key>
            <key type="filename">alienscene/star/star12.png</key>
            <key type="filename">alienscene/star/star13.png</key>
            <key type="filename">alienscene/star/star2.png</key>
            <key type="filename">alienscene/star/star3.png</key>
            <key type="filename">alienscene/star/star4.png</key>
            <key type="filename">alienscene/star/star5.png</key>
            <key type="filename">alienscene/star/star6.png</key>
            <key type="filename">alienscene/star/star7.png</key>
            <key type="filename">alienscene/star/star8.png</key>
            <key type="filename">alienscene/star/star9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>95,144,189,288</rect>
                <key>scale9Paddings</key>
                <rect>95,144,189,288</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/ui/blue_ui.png</key>
            <key type="filename">alienscene/ui/button_left.png</key>
            <key type="filename">alienscene/ui/button_left_c.png</key>
            <key type="filename">alienscene/ui/button_right.png</key>
            <key type="filename">alienscene/ui/button_right_c.png</key>
            <key type="filename">alienscene/ui/button_up.png</key>
            <key type="filename">alienscene/ui/button_up_c.png</key>
            <key type="filename">alienscene/ui/red_ui.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,59,117,117</rect>
                <key>scale9Paddings</key>
                <rect>59,59,117,117</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/ui/deuce.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,35,148,70</rect>
                <key>scale9Paddings</key>
                <rect>74,35,148,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">alienscene/ui/win.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>65,43,129,86</rect>
                <key>scale9Paddings</key>
                <rect>65,43,129,86</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>alienscene</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
