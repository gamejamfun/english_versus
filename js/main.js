var words = [
    'DOG',
    'CAT',
    'BEE',
    // 'FLY',
    // 'COW',
    // 'FOX',
    // 'ANT',
    // 'PIG',
    // 'RAT'
];

var config = {
    type: Phaser.AUTO,
    width: 900,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 600 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

var alien1, alien2, falling_star;
var platforms, aliens, letters, wordBoard;

function preload() {
    var progress = this.add.graphics();
    this.load.on('progress', function(value) {
        progress.clear();
        progress.fillStyle(0xffffff, 1);
        progress.fillRect(0, 270, 800 * value, 60);
    });

    this.load.on('complete', function() {
        progress.destroy();
    });

    this.load.setPath('assets/');
    this.load.audio('m_bgStart', 'audio/bg-start.mp3');
    this.load.audio('m_bgPlay', 'audio/bg-play.mp3');
    this.load.audio('m_jump1', 'audio/jump1.mp3');
    this.load.audio('m_jump2', 'audio/jump2.mp3');
    this.load.audio('m_boom', 'audio/boom.mp3');
    this.load.audio('m_eat', 'audio/eat.mp3');
    this.load.audio('m_play', 'audio/play.mp3');
    this.load.multiatlas('alienscene', 'alienscene.json', 'assets');
    this.load.image('jump_light_1', 'alienscene/light-1.png');
    this.load.image('jump_light_2', 'alienscene/light-2.png');
}

function create() {
    setupKeyboard(this);
    createAnimations(this);
    var background = this.add.sprite(0, 0, 'alienscene', 'background.png');
    platforms = this.physics.add.staticGroup();
    aliens = this.physics.add.group();
    var ground = this.add.rectangle(0, 630, 1800, 100, 0xffffff, 0);
    platforms.add(ground);

    alien1 = this.physics.add.sprite(50, 400, 'alienscene', 'alien/alien1-1.png').play('alien1_idle');
    alien1.setSize(120, 150, 60); // 校正碰撞位置
    alien1.setScale(0.5, 0.5);
    alien1.setVelocity(0, 300);
    alien1.jumpSound = this.sound.add('m_jump1', {
        volume: 0.3
    });
    alien1.isJump = false;
    alien1.scoreBoardBg = this.add.image(120, 110, 'alienscene', 'ui/blue_ui.png');
    alien1.scoreBoard = this.add.container(120, 110);
    //alien1.scoreBoard.setDisplaySize(1000,1000);
    alien1.collectLetters = [];

    alien2 = this.physics.add.sprite(850, 400, 'alienscene', 'alien/alien2-1.png').play('alien2_idle');
    alien2.setSize(100, 130, 50); // 校正碰撞位置
    alien2.setScale(0.6, 0.6);
    alien2.setVelocity(0, 300);
    alien2.jumpSound = this.sound.add('m_jump2', {
        volume: 0.3
    });
    alien2.isJump = false;
    alien2.scoreBoardBg = this.add.image(777, 110, 'alienscene', 'ui/red_ui.png');
    alien2.scoreBoard = this.add.container(780, 110);
    alien2.collectLetters = [];

    aliens.add(alien1);
    aliens.add(alien2);

    aliens.children.entries.forEach(function(alien){
        alien.setBounce(0.2);
        alien.setCollideWorldBounds(true);
    })

    //var aa = this.add.circle(100, 100, 30, 0x0ef00f);
    //var r1 = this.add.circle(200, 200, 80, 0x6666ff);


    this.physics.add.collider(aliens, platforms);
    // 流星雨
    falling_star = this.add.sprite(0, 0, 'alienscene', 'star/star1.png').on('animationcomplete', function(){
        var scale = Math.min(Math.random(), 0.5);
        this.setX(Math.random()*850 | 0);
        this.setY(Math.random()*250 | 0);
        this.setScale(scale, scale);
        var frameRate = Math.max(Math.min(Math.random() * 13, 13), 7) | 0;
        this.frameRate = frameRate;
        this.visible = true;
        this.play('falling_star');
    }).play('falling_star');
    // 背景音樂
    this.sound.add('m_bgStart', {
        mute: false,
        volume: 0.3,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
    }).play();
    //
    var particles = this.add.particles('jump_light_1');
    alien1.emitter = particles.createEmitter({
        speed: 100,
        scale: { start: 0.7, end: 0 },
        follow: alien1,
        followOffset: {
            x: 0,
            y: 45
        },
    });
    //alien1.emitter.startFollow(alien1, 0, 45, 1);
    alien1.emitter.stop();//
    wordBoard = this.add.container(400, 50);
    start(this);
}

function update(time, delta) {
    // alien1.x += delta/8;
    // if(alien1.y > 600)
    // {
    //     alien1.y = 550;
    // }
    // console.log(alien1.y)

    // alien2.x -= delta/8;
    // if(alien2.x < 50)
    // {
    //     alien2.x = 900;
    // }

    if (SpaceKey.isDown && !alien1.isJump)
    {
        alien1.isJump = true;
        alien1.emitter.start();
        alien1.jumpSound.play();
        alien1.body.setVelocityY(-200);
        setTimeout(function() {
            alien1.emitter.stop();
        }, 100);
    }

    if (SpaceKey.isUp)
    {
        alien1.isJump = false;
    }

    if (AKey.isDown) {
        alien1.setVelocityX(-250);
    }else if (DKey.isDown) {
        alien1.setVelocityX(250);
    } else {
        alien1.setVelocityX(0);
    }
}

// 建立動畫影格
function createAnimations(self) {
    var alien1Frame = self.anims.generateFrameNames('alienscene', {
        start: 1, end: 3, zeroPad: 0,
        prefix: 'alien/alien1-', suffix: '.png'
    });

    self.anims.create({ key: 'alien1_idle', frames: alien1Frame, frameRate: 10, repeat: -1 });
    

    var alien2Frame = self.anims.generateFrameNames('alienscene', {
        start: 1, end: 3, zeroPad: 0,
        prefix: 'alien/alien2-', suffix: '.png'
    });

    self.anims.create({ key: 'alien2_idle', frames: alien2Frame, frameRate: 10, repeat: -1 });

    // 流星

    var fallingStar = self.anims.generateFrameNames('alienscene', {
        start: 1, end: 13, zeroPad: 0,
        prefix: 'star/star', suffix: '.png'
    });

    self.anims.create({ key: 'falling_star', frames: fallingStar, frameRate: 9, repeat: 0, hideOnComplete: true});

    // 吃到字母
    var eatingEffect = self.anims.generateFrameNames('alienscene', {
        start: 0, end: 7, zeroPad: 0,
        prefix: 'explosion/explosion', suffix: '.png'
    });

    self.anims.create({ key: 'eating_effect', frames: eatingEffect, frameRate: 9, repeat: 0, hideOnComplete: true});
    // Dog
    var dogAttack = self.anims.generateFrameNames('alienscene', {
        start: 1, end: 3, zeroPad: 0,
        prefix: 'animals/dog/dog-0', suffix: '.png'
    });

    self.anims.create({ key: 'dog_attack', frames: dogAttack, frameRate: 3, repeat: -1, hideOnComplete: true});
}

// 鍵盤按鍵
function setupKeyboard(self) {
    self.input.keyboard.on('keydown', function (event) {

        // console.dir(event);
        //alien1.y -= 10;
    });

    //  Hook to a specific key without creating a new Key object (in this case the A key)

    self.input.keyboard.on('keydown_A', function (event) {

        // console.log('Hello from the A Key!');

    });

    //  Fire only once on a specific key up event (in this case the S key)

    self.input.keyboard.on('keyup_S', function (event) {

        // console.log('Keyboard Events Stopped');

        //self.input.keyboard.stopListeners();

    }, self);

    //  Create a Key object we can poll directly in a tight loop

    AKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    DKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    SpaceKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
}

// 更新拼字版
function refreshPuzzle(self, alien) {
    alien.scoreBoard.removeAll();
    alien.collectLetters.forEach(function($r, $idx){
        alien.scoreBoard.add(game.scene.scenes[0].add.image(-74 + (36.5 * $idx), 5.5, 'alienscene', 'font/'+$r+'.png').setScale(0.4, 0.4));
    });
    checkPuzzle(self, alien);
}

// 檢查拼字
function checkPuzzle(self, alien) {
    var wordLetter = wordBoard.word.split('');
    alien.collectLetters.forEach(function($w, $idx){
        if (wordLetter[$idx] != $w) {
            alien.scoreBoard.removeAll();
            alien.collectLetters = [];
        }
    });
    if (alien.collectLetters.join('') == wordBoard.word) {
        var attack = self.physics.add.sprite(alien.x, alien.y, 'alienscene', 'animals/dog/dog-01.png').play('dog_attack');
        attack.setScale(0.5, 0.5);
        attack.body.setAllowGravity(false);
        attack.body.setVelocityX(600);
        self.physics.add.collider(attack, alien2, function(attack, alien2){
            attack.setVisible(false);
            attack.disableBody();
            attack.destroy();
        });
        console.log(attack);

        genWord(self, letters);
    }

}

// 初始化單字
function start(self) {
    //
    letters = self.physics.add.group();
    letters.eatingSound = self.sound.add('m_eat', {
        volume: 0.3
    });
    genWord(self, letters);
    //
    self.physics.add.overlap(aliens, letters, function (alien, letter) {
        letters.eatingSound.play();
        var ex = self.add.sprite(letter.x, letter.y, 'alienscene', 'explosion/explosion0.png').play('eating_effect');
        ex.setScale(0.5, 0.5);
        letter.setVisible(false);
        letter.disableBody();
        letter.body.reset(0, 0);
        //
        alien.collectLetters = [...alien.collectLetters, letter.letter];
        //
        refreshPuzzle(self, alien);
        
    });
    // ground
    self.physics.add.overlap(platforms, letters, function (platforms, letter) {
        letter.setVisible(false);
        letter.disableBody();
        letter.body.reset(0, 0);
        
    });

    setInterval(function(){
        var idx = Math.random() * letters.children.size | 0,
            letter = letters.children.entries[idx];
        if (letter.visible) {
            idx = Math.random() * letters.children.size | 0,
            letter = letters.children.entries[idx];
            console.log('re-gen');
        }
        letter.setX((Math.random()*850)+30 | 0);
        letter.setVisible(true);
        letter.enableBody();
        
    }, 800);
}

// 產生拼字板
function genWord(self, letters) {
    letters.clear();
    wordBoard.removeAll();
    alien1.scoreBoard.removeAll();
    alien2.scoreBoard.removeAll();
    alien1.collectLetters = [];
    alien2.collectLetters = [];
    //
    var idx = Math.random() * words.length | 0;
    wordBoard.word = words[idx];
    wordBoard.word.split('').forEach(function($r, $idx){
        wordBoard.add(wordBoard.scene.add.image((50 * $idx), 5.5, 'alienscene', 'font/'+$r+'.png').setTint(0x00ff00));
    });
    //
    wordBoard.word.split('').forEach(function($l){
        var img = self.physics.add.image(0, 0, 'alienscene', 'font/'+$l+'.png');
        img.letter = $l;
        img.disableBody();
        img.setVisible(false);
        img.body.setVelocity(0,0);
        letters.add(img);
    });

}